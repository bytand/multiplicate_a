# multiplicate_A

Условия

Необходимо написать программу с функцией multiplicate(A), принимающей на вход массив целых чисел А ненулевой длины и массив такой же длины, в котором на i-ом месте находится произведение всех чисел массива А, кроме числа, стоящего на i-ом месте.

Язык программирования: Python.

Использование дополнительных библиотек и функций: не разрешается.

В качестве решения необходимо прислать ссылку на GitHub.

Пример

На вход подается массив [1, 2, 3, 4]

На выходе функции ожидается массив [24, 12, 8, 6]

Задача можно решить, следующим образом: вычслить произведение всех элементов, разделив его на i-ый элемент исходного массива)))Но тут есть один ньюанс: если в массиве нет нулевых элементов, то все отработает хорошо, если нулей больше чем один, тоже все хорошо, но если ноль только единственный, то данный алгоритм соврет, несмотря на все свои преимущества - простоту и быстроту)))тогда целесообразно, если ноль единственный, вычислить произведения до нулевого элемента, и после...Конечно, это ни так быстро, как в превом случае, но за то более точно)))  Результат в проекте multiplicate_B)))